package uk.co.hexeptionclients.jds.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import uk.co.hexeptionclients.jds.wrapper.Wrapper;

public abstract class Cmd {
	
	private String name = getClass().getAnnotation(CmdInfo.class).name();
	private String help = getClass().getAnnotation(CmdInfo.class).help();
	private String[] syntax = getClass().getAnnotation(CmdInfo.class).syntax();
	
	public static String cmdPrefix = ".";
	
	@Retention(RetentionPolicy.RUNTIME)
	public @interface CmdInfo {
		String name();
		
		String help();
		
		String[] syntax();
	}
	
	public String getCmdName() {
		return name;
	}
	
	public String getHelp() {
		return help;
	}
	
	public void setHelp(String help) {
		this.help = help;
	}
	
	public final String getName() {
		return cmdPrefix + name;
	}
	
	public String[] getSyntax() {
		return syntax;
	}
	
	public class Error extends Throwable {
		public Error() {
			super();
		}
		
		public Error(String message) {
			super(message);
		}
	}
	
	public class SyntaxError extends Error {
		public SyntaxError() {
			super();
		}
		
		public SyntaxError(String message) {
			super(message);
		}
	}
	
	protected final void syntaxError() throws SyntaxError {
		throw new SyntaxError();
	}
	
	protected final void syntaxError(String message) throws SyntaxError {
		throw new SyntaxError(message);
	}
	
	protected final void error(String message) throws Error {
		throw new Error(message);
	}
	
	public abstract void execute(String[] args) throws Error;
	
	public final void printSyntax() {
		String output = "�o" + cmdPrefix + name + "�r";
		if (syntax.length != 0) {
			output += " " + syntax[0];
			for (int i = 1; i < syntax.length; i++) {
				output += "\n    " + syntax[i];
			}
		}
		
		for (String line : output.split("\n")) {
			Wrapper.getInstance().addChatMessage(line);
		}
		
	}
	
	public final void printHelp(){
		for(String line : help.split("\n")){
			Wrapper.getInstance().addChatMessage(line);
		}
	}
	
}
