package uk.co.hexeptionclients.jds.fonts;

import java.awt.Font;

public class JDSFontManager {
	
	public static JDSFont Arial45;
	public static JDSFont Arial35;
	public static JDSFont Arial25;
	public static JDSFont Arial20;
	
	public static void loadFonts(){
		Arial45 = new JDSFont("Arial", Font.PLAIN, 45);
		Arial35 = new JDSFont("Arial", Font.PLAIN, 35);
		Arial25 = new JDSFont("Arial", Font.PLAIN, 25);
		Arial20 = new JDSFont("Arial", Font.PLAIN, 20);
	}
	
}
