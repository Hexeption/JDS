package uk.co.hexeptionclients.jds.gui.keybinds;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import uk.co.hexeptionclients.jds.JDS;

public class GuiKeybindChange extends GuiScreen implements GuiPressAKeyCallback {
	
	private GuiScreen prevScreen;
	private GuiTextField textfield;
	private Entry<String, TreeSet<String>> entry;
	private String key = "NONE";
	
	public GuiKeybindChange(GuiScreen prevScreen, Entry<String, TreeSet<String>> entry) {
		this.prevScreen = prevScreen;
		this.entry = entry;
		if (entry != null) {
			key = entry.getKey();
		}
	}
	
	@Override
	public void updateScreen() {
		textfield.updateCursorCounter();
	}
	
	@Override
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		buttonList.add(new GuiButton(0, width / 2 - 100, 60, "Change Key"));
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 72, "Save"));
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 97, "Cancel"));
		
		textfield = new GuiTextField(0, fontRendererObj, width / 2 - 100, 100, 200, 20);
		textfield.setMaxStringLength(65536);
		textfield.setFocused(true);
		
		if (entry != null) {
			String keybinds = "";
			for (String key : entry.getValue()) {
				if (!keybinds.isEmpty()) {
					keybinds += ";";
				}
				textfield.setText(keybinds);
			}
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.enabled)
			if (button.id == 0)
				mc.displayGuiScreen(new GuiPressAKey(this));
			else if (button.id == 1) {
				if (entry != null)
					JDS.theClient.keybindsManager.remove(entry.getKey());
				JDS.theClient.keybindsManager.put(key, new TreeSet<String>(Arrays.asList(textfield.getText().split(";"))));
				// WurstClient.INSTANCE.files.saveKeybinds();
				mc.displayGuiScreen(prevScreen);
			} else if (button.id == 2)
				mc.displayGuiScreen(prevScreen);
	}
	
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		textfield.textboxKeyTyped(typedChar, keyCode);
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);
		textfield.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawBackground(0);
		drawCenteredString(fontRendererObj, (entry != null ? "Edit" : "Add") + " Keybind", width / 2, 20, 16777215);
		drawString(fontRendererObj, "Key: " + key, width / 2 - 100, 47, 10526880);
		drawString(fontRendererObj, "Commands (separated by \";\")", width / 2 - 100, 87, 10526880);
		textfield.drawTextBox();
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
}
