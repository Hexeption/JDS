package uk.co.hexeptionclients.jds.gui.keybinds;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.GuiScreen;

class GuiPressAKey extends GuiScreen {
	private GuiPressAKeyCallback prevScreen;
	
	public GuiPressAKey(GuiPressAKeyCallback prevScreen) {
		if (!(prevScreen instanceof GuiScreen)) {
			throw new IllegalArgumentException("prevScreen is not an instance of GuiScreen");
		}
		this.prevScreen = prevScreen;
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		prevScreen.setKey(Keyboard.getKeyName(keyCode));
		mc.displayGuiScreen((GuiScreen) this.prevScreen);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawBackground(0);
		drawCenteredString(fontRendererObj, "Press a key", width / 2, height / 4 + 48, 16777215);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
}
