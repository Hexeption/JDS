package uk.co.hexeptionclients.jds.gui.keybinds;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlot;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.module.Module;

public class GuiKeybindSlot extends GuiSlot {
	
	private GuiScreen lastScreen;
	private Minecraft mc;
	
	private int selectedSlot;
	
	public GuiKeybindSlot(Minecraft mcIn, GuiScreen lastscreen) {
		super(mcIn, lastscreen.width, lastscreen.height, 36, lastscreen.height - 56, 30);
		mc = mcIn;
	}
	
	@Override
	protected boolean isSelected(int id) {
		return selectedSlot == id;
	}
	
	protected int getSelectedSlot() {
		return selectedSlot;
	}
	
	@Override
	protected int getSize() {
		return JDS.theClient.keybindsManager.size();
	}
	
	@Override
	protected void elementClicked(int var1, boolean var2, int var3, int var4) {
		selectedSlot = var1;
	}
	
	@Override
	protected void drawBackground() {
	}
	
	@Override
	protected void updateItemPos(int entryID, int insideLeft, int yPos) {
		super.updateItemPos(entryID, insideLeft, yPos);
	}
	
	@Override
	protected void drawSlot(int entryID, int insideLeft, int yPos, int insideSlotHeight, int mouseXIn, int mouseYIn) {
		Entry entry = JDS.theClient.keybindsManager.entrySet().toArray(new Map.Entry[JDS.theClient.keybindsManager.size()])[entryID];
		mc.fontRendererObj.drawString("Key: " + entry.getKey(), insideLeft + 3, yPos + 3, 10526880);
		mc.fontRendererObj.drawString("Command: " + entry.getValue(), insideLeft + 3, yPos + 15, 10526880);
	}
	
}
