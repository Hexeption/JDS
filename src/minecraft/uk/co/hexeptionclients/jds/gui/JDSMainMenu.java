package uk.co.hexeptionclients.jds.gui;

import java.io.IOException;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import uk.co.hexeptionclients.jds.JDS;

public class JDSMainMenu extends GuiMainMenu {
	
	private static final ResourceLocation MINECRAFT_TITLE_TEXTURES = new ResourceLocation("jds/JDS.png");
	
	@Override
	public void initGui() {
		super.initGui();
		// TODO:
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		
		switch (button.id) {
			case 14:
				// mc.displayGuiScreen(new AltManager(this));
				break;
			case 15:
				 mc.displayGuiScreen(new JDSOptions(this));
				break;
		}
	}
	
	@Override
	public void updateScreen() {
		super.updateScreen();
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		GlStateManager.disableAlpha();
		renderSkybox(mouseX, mouseY, partialTicks);
		GlStateManager.enableAlpha();
		drawGradientRect(0, 0, width, height, -2130706433, 16777215);
		drawGradientRect(0, 0, width, height, 0, Integer.MIN_VALUE);
		int i = 274;
		int j = this.width / 2 - 137;
		int k = 30;
		mc.getTextureManager().bindTexture(MINECRAFT_TITLE_TEXTURES);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		drawModalRectWithCustomSizedTexture(width / 2 - 256 / 2, 36, 0, 0, 256, 64, 256, 64);
		
		String Minecraft = "Minecraft 1.10.2";
		String Minecraft1 = "Copyright Mojang AB";
		String Minecraft2 = "Do not distribute!";
		drawString(fontRendererObj, Minecraft, this.width - fontRendererObj.getStringWidth(Minecraft) - 5, this.height - 29, 0xffffff);
		drawString(fontRendererObj, Minecraft1, this.width - fontRendererObj.getStringWidth(Minecraft1) - 5, this.height - 19, 0xffffff);
		drawString(fontRendererObj, Minecraft2, this.width - fontRendererObj.getStringWidth(Minecraft2) - 5, this.height - 9, 0xffffff);
		
		drawString(fontRendererObj, JDS.theClient.clientName + " " + JDS.theClient.clientVersion, 5, this.height - 29, 0xffffff);
		drawString(fontRendererObj, "Copyright Hexeption Clients (Keir)", 5, this.height - 19, 0xffffff);
		drawString(fontRendererObj, "All rights reserved.", 5, this.height - 9, 0xffffff);
		
		for(Object button : buttonList){
			((GuiButton) button).drawButton(mc, mouseX, mouseY);
		}
	}
	
}
