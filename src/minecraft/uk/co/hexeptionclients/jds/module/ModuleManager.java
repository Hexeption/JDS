package uk.co.hexeptionclients.jds.module;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeMap;

public class ModuleManager {
	
	private final TreeMap<String, Module> mods = new TreeMap<String, Module>(new Comparator<String>() {
		
		@Override
		public int compare(String o1, String o2) {
			return o1.compareToIgnoreCase(o2);
		}
	});
	
	public final StepMod stepMod = new StepMod();
	public final IRCMod ircMod = new IRCMod();
	public final SprintMod sprintMod = new SprintMod();
	
	public ModuleManager() {
		try {
			for (Field field : ModuleManager.class.getFields()) {
				if (field.getName().endsWith("Mod")) {
					Module mod = (Module) field.get(this);
					mods.put(mod.getName(), mod);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Collection<Module> getAllMods() {
		return mods.values();
	}
	
	public Module getModByName(String name) {
		return mods.get(name);
	}
	
	public int countMods() {
		return mods.size();
	}
	
}
