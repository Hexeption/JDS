package uk.co.hexeptionclients.jds.ui;

import net.minecraft.client.Minecraft;

public interface InGameHud {
	
	public abstract String themeName();
	
	public abstract void render(Minecraft minecraft, int displayWidth, int dispalyHeight);
	
	public abstract void onKeyPressed(int key);
	
}
